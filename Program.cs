﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using YamlDotNet.RepresentationModel;

namespace enp
{
    public static class Program
    {
        private static void Main()
        {
            //Загрузка параметров из CONFIGFILE.YAML
			var input = new StreamReader("CONFIGFILE.YAML");
			var yaml = new YamlStream();
			yaml.Load(input);
			var mapping = (YamlMappingNode)yaml.Documents[0].RootNode;
            //Установка параметров
            int win_width = int.Parse(((YamlScalarNode)mapping["Graphics settings"]["resolution"]["width"]).Value);
            int win_height = int.Parse(((YamlScalarNode)mapping["Graphics settings"]["resolution"]["height"]).Value);
            int smooth = int.Parse(((YamlScalarNode)mapping["Graphics settings"]["smooth"]).Value);; // зглаживание
// Создает 3.0-совместимый GraphicsContext с 32bpp цвет, глубина 24bpp
// 8bpp трафарет и 4x сглаживание
            GraphicsMode GCfg =  new GraphicsMode(32, 24, 8, smooth);

            using (var window = new Window(win_width,win_height,GCfg, "enp"))
            {
                window.Run(60.0);
            }

            // And that's it! That's all it takes to create a window with OpenTK.
        }
    }
}
