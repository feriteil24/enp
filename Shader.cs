﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
namespace enp
{
    public class Shader
    {
        int Handle;

         public Shader(string vertexPath, string fragmentPath)
        {
            // Читаем шейдеры
            string VertexShaderSource; // Вершиный шейдер
            using (StreamReader reader = new StreamReader(vertexPath, Encoding.UTF8))
            {
                VertexShaderSource = reader.ReadToEnd();
            }

            string FragmentShaderSource; // Фрагмениный шейдер(цвет)
            using (StreamReader reader = new StreamReader(fragmentPath, Encoding.UTF8))
            {
                FragmentShaderSource = reader.ReadToEnd();
            }
            // Создаём шейдеры
            var VertexShader = GL.CreateShader(ShaderType.VertexShader); // Вершины
            GL.ShaderSource(VertexShader, VertexShaderSource);

            var FragmentShader = GL.CreateShader(ShaderType.FragmentShader); // Фрагменты
            GL.ShaderSource(FragmentShader, FragmentShaderSource);

            // Компилируем
            GL.CompileShader(VertexShader); //Вершины
            string infoLogVert = GL.GetShaderInfoLog(VertexShader);
            if (infoLogVert != System.String.Empty) System.Console.WriteLine(infoLogVert);

            GL.CompileShader(FragmentShader); // Фрагменты
            string infoLogFrag = GL.GetShaderInfoLog(FragmentShader);
            if (infoLogFrag != System.String.Empty) System.Console.WriteLine(infoLogFrag);

            // Создаём програму с шейдерами на GPU
            Handle = GL.CreateProgram();

            GL.AttachShader(Handle, VertexShader);
            GL.AttachShader(Handle, FragmentShader);

            GL.LinkProgram(Handle);

            // Почистим
            GL.DetachShader(Handle, VertexShader);
            GL.DetachShader(Handle, FragmentShader);
            GL.DeleteShader(FragmentShader);
            GL.DeleteShader(VertexShader);
        }
        // Видемость для вызова
        public void Use() 
        {
            GL.UseProgram(Handle);
        }
        // Очистка
        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {   
            if (!disposedValue)
            {
                GL.DeleteProgram(Handle);

                disposedValue = true;
            }
        }
        ~Shader()
        {
            GL.DeleteProgram(Handle);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}