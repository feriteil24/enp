﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
namespace enp
{
    public class Window : GameWindow
    {
        float[] vertices = {
            -0.5f, -0.5f, 0.0f, //Bottom-left vertex
            0.5f, -0.5f, 0.0f, //Bottom-right vertex
            0.0f,  0.5f, 0.0f  //Top vertex
            };

    private Shader shader;

    int VertexBufferObject; // Буфер вершин
    int VertexArrayObject; // Буфер 
       protected override void OnLoad(EventArgs e) // Загрузка окна
        {
        // Инициализация 
         GL.ClearColor(0.5f, 0.5f, 0.5f, 1.0f);
         VertexBufferObject = GL.GenBuffer();
         GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
         GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
         shader = new Shader("Shaders/shader.vert", "Shaders/shader.frag");

       GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
        GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);
            shader.Use();
        

         base.OnLoad(e);
        }
        public Window(int width, int height,GraphicsMode preste_grafical, string title) : base(width, height, preste_grafical, title)
        {
            
        }
        protected override void OnRenderFrame(FrameEventArgs e) // Вывод кадра на экран
        {
            GL.Clear(ClearBufferMask.ColorBufferBit); // Очистка предидущего буфера (кадра)

            shader.Use();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
            
            SwapBuffers();  // Меняем местами текущий буфер и "подготовленный"
            base.OnRenderFrame(e);
        }

        protected override void OnResize(EventArgs e) //Если изминился размер окна
        {
            GL.Viewport(0, 0, Width, Height);
            base.OnResize(e);
        }
         protected override void OnUpdateFrame(FrameEventArgs e) // Цикл обновления окна
        {
            var input = Keyboard.GetState();

            if (input.IsKeyDown(Key.Escape))
            {
                Exit();
            }
            
             base.OnUpdateFrame(e);
        }
        protected override void OnUnload(EventArgs e) //Остановка программы
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferObject);
            shader.Dispose();
            base.OnUnload(e);
        }
    }
}
